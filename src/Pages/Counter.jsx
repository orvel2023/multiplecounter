import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeCounterListToSub,changeCounterListToAdd } from "../redux/reducer/counterList";

export default function Counter(props){
    const { index } =  props;
    const dispatch = useDispatch()
    const counterList = useSelector((store) => {
      return store.counterList.value
        })
    const clickSubCount = () => {
        dispatch(changeCounterListToSub(index))
    }
    const clickAddCount = () => {
        dispatch(changeCounterListToAdd(index))
    }
    return (
        <div>
            <button onClick={clickAddCount}>
                +
            </button>
            <span style={{margin:10}}>
                 {counterList[index]}
            </span>
            <button onClick={clickSubCount}>
                -
            </button>
        </div>
    )
}