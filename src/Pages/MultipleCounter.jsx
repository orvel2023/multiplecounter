import React from "react";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroupSum from "./CounterGroupSum";
import CounterGroup from "./CounterGroup";

export default function MultipleCounter(){
    return (
        <div>
            <CounterSizeGenerator></CounterSizeGenerator>

            <CounterGroupSum></CounterGroupSum>

            <CounterGroup></CounterGroup>
        </div>
    )
}