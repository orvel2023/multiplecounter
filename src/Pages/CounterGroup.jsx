import React from "react";
import Counter from "./Counter";
import {  useSelector } from "react-redux";
import shortid from 'shortid'

export default function CounterGroup(){
    const counterList = useSelector((store) => {
        return store.counterList.value
    })
     return (
        <div>
             {
                counterList.map((item,index) => {
                    return (<Counter key={shortid.generate()}  index = {index} /> );
                })
            }
        </div>
    )
}