
import React from "react";
import {  useSelector,useDispatch } from "react-redux";
import { changecounterListSize } from "../redux/reducer/counterList";
export default function CounterSizeGenerator() {
    const dispatch = useDispatch()
    const counterList = useSelector((store) => {
        return store.counterList.value
    })
    const setSize = (event) => {
        dispatch(changecounterListSize(event.target.value))
    }
     return (
        <div>
            Size: <input type="number" value={counterList.length || 0} onChange={setSize}/>
        </div>
        
    )
}