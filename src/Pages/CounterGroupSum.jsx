import React from "react";
import { useSelector } from "react-redux";
export default function CounterGroupSum() {
    const counterList = useSelector((store) => {
        return store.counterList.value
    })
    return (
        <div>
            Sum : { counterList.reduce((sum,value) => {
                return sum + value;
            },0) }
        </div>
    )
}