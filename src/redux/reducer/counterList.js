import {createSlice} from '@reduxjs/toolkit'
const counterListSlice =  createSlice({
    name: 'counterList',
    initialState: {
      value: []
    },
    reducers: {
      changeCounterListToAdd: (state,actions) => {
        state.value[actions.payload] += 1
      },
      changecounterListSize: (state,actions) => {
        const list = actions.payload ? Array.from({ length: actions.payload }).fill(0) : [];
        state.value = list
      },
      changeCounterListToSub: (state,actions) => {
        state.value[actions.payload] -= 1
      },
    }
  })
export const { changeCounterListToAdd,changecounterListSize,changeCounterListToSub } = counterListSlice.actions;
export default counterListSlice;