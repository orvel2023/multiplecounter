import { configureStore } from '@reduxjs/toolkit'
import counterListSlice from './reducer/counterList';

const store =  configureStore({
    reducer: {counterList:counterListSlice.reducer}
});
export default store