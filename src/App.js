import './App.css';
import MultipleCounter from './Pages/MultipleCounter';

function App() {
  return (
    <div className="App">
      <MultipleCounter/>
    </div>
  );
}

export default App;
